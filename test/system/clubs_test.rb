require "application_system_test_case"

class ClubsTest < ApplicationSystemTestCase
  setup do
    @club = clubs(:one)
  end

  test "visiting the index" do
    visit clubs_url
    assert_selector "h1", text: "Clubs"
  end

  test "creating a Club" do
    visit clubs_url
    click_on "New Club"

    fill_in "Draw", with: @club.draw
    fill_in "From", with: @club.from
    fill_in "Lose", with: @club.lose
    fill_in "Managemantcv", with: @club.managemantcv_id
    fill_in "Matches", with: @club.matches
    fill_in "Position", with: @club.position
    fill_in "Until", with: @club.until
    fill_in "Win", with: @club.win
    click_on "Create Club"

    assert_text "Club was successfully created"
    click_on "Back"
  end

  test "updating a Club" do
    visit clubs_url
    click_on "Edit", match: :first

    fill_in "Draw", with: @club.draw
    fill_in "From", with: @club.from
    fill_in "Lose", with: @club.lose
    fill_in "Managemantcv", with: @club.managemantcv_id
    fill_in "Matches", with: @club.matches
    fill_in "Position", with: @club.position
    fill_in "Until", with: @club.until
    fill_in "Win", with: @club.win
    click_on "Update Club"

    assert_text "Club was successfully updated"
    click_on "Back"
  end

  test "destroying a Club" do
    visit clubs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Club was successfully destroyed"
  end
end
