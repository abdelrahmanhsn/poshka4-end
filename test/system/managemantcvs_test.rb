require "application_system_test_case"

class ManagemantcvsTest < ApplicationSystemTestCase
  setup do
    @managemantcv = managemantcvs(:one)
  end

  test "visiting the index" do
    visit managemantcvs_url
    assert_selector "h1", text: "Managemantcvs"
  end

  test "creating a Managemantcv" do
    visit managemantcvs_url
    click_on "New Managemantcv"

    fill_in "Agent", with: @managemantcv.agent
    fill_in "Born", with: @managemantcv.born
    fill_in "Ch Fb", with: @managemantcv.ch_fb
    fill_in "Ch Tw", with: @managemantcv.ch_tw
    fill_in "Ch Yt", with: @managemantcv.ch_yt
    fill_in "Competition", with: @managemantcv.competition
    fill_in "Current Club", with: @managemantcv.current_club
    fill_in "Ending Contract", with: @managemantcv.ending_contract
    fill_in "Gender", with: @managemantcv.gender
    fill_in "Name", with: @managemantcv.name
    fill_in "Photo", with: @managemantcv.photo
    fill_in "Tier", with: @managemantcv.tier
    click_on "Create Managemantcv"

    assert_text "Managemantcv was successfully created"
    click_on "Back"
  end

  test "updating a Managemantcv" do
    visit managemantcvs_url
    click_on "Edit", match: :first

    fill_in "Agent", with: @managemantcv.agent
    fill_in "Born", with: @managemantcv.born
    fill_in "Ch Fb", with: @managemantcv.ch_fb
    fill_in "Ch Tw", with: @managemantcv.ch_tw
    fill_in "Ch Yt", with: @managemantcv.ch_yt
    fill_in "Competition", with: @managemantcv.competition
    fill_in "Current Club", with: @managemantcv.current_club
    fill_in "Ending Contract", with: @managemantcv.ending_contract
    fill_in "Gender", with: @managemantcv.gender
    fill_in "Name", with: @managemantcv.name
    fill_in "Photo", with: @managemantcv.photo
    fill_in "Tier", with: @managemantcv.tier
    click_on "Update Managemantcv"

    assert_text "Managemantcv was successfully updated"
    click_on "Back"
  end

  test "destroying a Managemantcv" do
    visit managemantcvs_url
    page.accept_confirm do
      click_on "Destroy", match: :first
    end

    assert_text "Managemantcv was successfully destroyed"
  end
end
