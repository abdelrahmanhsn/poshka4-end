require 'test_helper'

class ManagemantcvsControllerTest < ActionDispatch::IntegrationTest
  setup do
    @managemantcv = managemantcvs(:one)
  end

  test "should get index" do
    get managemantcvs_url
    assert_response :success
  end

  test "should get new" do
    get new_managemantcv_url
    assert_response :success
  end

  test "should create managemantcv" do
    assert_difference('Managemantcv.count') do
      post managemantcvs_url, params: { managemantcv: { agent: @managemantcv.agent, born: @managemantcv.born, ch_fb: @managemantcv.ch_fb, ch_tw: @managemantcv.ch_tw, ch_yt: @managemantcv.ch_yt, competition: @managemantcv.competition, current_club: @managemantcv.current_club, ending_contract: @managemantcv.ending_contract, gender: @managemantcv.gender, name: @managemantcv.name, photo: @managemantcv.photo, tier: @managemantcv.tier } }
    end

    assert_redirected_to managemantcv_url(Managemantcv.last)
  end

  test "should show managemantcv" do
    get managemantcv_url(@managemantcv)
    assert_response :success
  end

  test "should get edit" do
    get edit_managemantcv_url(@managemantcv)
    assert_response :success
  end

  test "should update managemantcv" do
    patch managemantcv_url(@managemantcv), params: { managemantcv: { agent: @managemantcv.agent, born: @managemantcv.born, ch_fb: @managemantcv.ch_fb, ch_tw: @managemantcv.ch_tw, ch_yt: @managemantcv.ch_yt, competition: @managemantcv.competition, current_club: @managemantcv.current_club, ending_contract: @managemantcv.ending_contract, gender: @managemantcv.gender, name: @managemantcv.name, photo: @managemantcv.photo, tier: @managemantcv.tier } }
    assert_redirected_to managemantcv_url(@managemantcv)
  end

  test "should destroy managemantcv" do
    assert_difference('Managemantcv.count', -1) do
      delete managemantcv_url(@managemantcv)
    end

    assert_redirected_to managemantcvs_url
  end
end
