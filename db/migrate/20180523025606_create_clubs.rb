class CreateClubs < ActiveRecord::Migration[5.2]
  def change
    create_table :clubs do |t|
      t.date :from
      t.date :until
      t.string :position
      t.integer :matches
      t.integer :win
      t.integer :draw
      t.integer :lose
      t.references :managemantcv, foreign_key: true

      t.timestamps
    end
  end
end
