class CreateManagemantcvs < ActiveRecord::Migration[5.2]
  def change
    create_table :managemantcvs do |t|
      t.string :name
      t.string :gender
      t.string :photo
      t.date :born
      t.string :current_club
      t.string :competition
      t.string :tier
      t.string :ending_contract
      t.string :agent
      t.string :ch_fb
      t.string :ch_yt
      t.string :ch_tw

      t.timestamps
    end
  end
end
