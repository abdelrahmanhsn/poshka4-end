class ManagemantcvsController < ApplicationController
  before_action :set_managemantcv, only: [:show, :edit, :update, :destroy]

  # GET /managemantcvs
  # GET /managemantcvs.json
  def index
    @managemantcvs = Managemantcv.all
  end

  # GET /managemantcvs/1
  # GET /managemantcvs/1.json
  def show
  end

  # GET /managemantcvs/new
  def new
    @managemantcv = Managemantcv.new
  end

  # GET /managemantcvs/1/edit
  def edit
  end

  # POST /managemantcvs
  # POST /managemantcvs.json
  def create
    @managemantcv = Managemantcv.new(managemantcv_params)

    respond_to do |format|
      if @managemantcv.save
        format.html { redirect_to @managemantcv, notice: 'Managemantcv was successfully created.' }
        format.json { render :show, status: :created, location: @managemantcv }
      else
        format.html { render :new }
        format.json { render json: @managemantcv.errors, status: :unprocessable_entity }
      end
    end
  end

  # PATCH/PUT /managemantcvs/1
  # PATCH/PUT /managemantcvs/1.json
  def update
    respond_to do |format|
      if @managemantcv.update(managemantcv_params)
        format.html { redirect_to @managemantcv, notice: 'Managemantcv was successfully updated.' }
        format.json { render :show, status: :ok, location: @managemantcv }
      else
        format.html { render :edit }
        format.json { render json: @managemantcv.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /managemantcvs/1
  # DELETE /managemantcvs/1.json
  def destroy
    @managemantcv.destroy
    respond_to do |format|
      format.html { redirect_to managemantcvs_url, notice: 'Managemantcv was successfully destroyed.' }
      format.json { head :no_content }
    end
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_managemantcv
      @managemantcv = Managemantcv.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def managemantcv_params
      params.require(:managemantcv).permit(:name, :gender, :photo, :born, :current_club, :competition, :tier, :ending_contract, :agent, :ch_fb, :ch_yt, :ch_tw)
    end
end
