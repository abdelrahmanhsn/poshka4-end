class ClubsController < ApplicationController
    def create
      @managemantcv = Managemantcv.find(params[:managemantcv_id])
      @club = Club.create(params[:club].permit(:body))
      @club.user_id = current_lawyer.id
      @club.managemantcv_id = @managemantcv.id
      @club.save

      respond_to do |format|
        puts params[:managemantcv_id]
        format.html { redirect_to @managemantcv }
       end
    end

    def destroy
           @managemantcv = Managemantcv.find(params[:managemantcv_id])
           @club = Club.find(params[:id])
           @club.destroy

           respond_to do |format|
               format.html { redirect_to @managemantcv }

           end
       end

      def edit
      end

       def update
           @managemantcv = Managemantcv.find(params[:managemantcv_id])
           @club = @managemantcv.clubs.find(params[:id])

           if @club.update(clubs_params)
             redirect_to managemantcv_path(@managemantcv)
           else
             render 'edit'
           end
    end
  private
       def club_params
          params.require(:club).permit(:body)
      end
  end
