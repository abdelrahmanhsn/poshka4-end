json.extract! managemantcv, :id, :name, :gender, :photo, :born, :current_club, :competition, :tier, :ending_contract, :agent, :ch_fb, :ch_yt, :ch_tw, :created_at, :updated_at
json.url managemantcv_url(managemantcv, format: :json)
